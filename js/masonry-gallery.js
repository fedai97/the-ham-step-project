﻿$(document).ready(function () {
    const $grid = $('.grid').masonry({
        columnWidth: 370,
        itemSelector: '.grid-item',
        percentPosition: true,
        gutter: 17
    });

    $('.wrapper-gallery .load-more').on('click', function () {
        const elems = [getItemElement(), getItemElement(), getItemElement()];
        const $elems = $(elems);
        const animationDiv = $('.wrapper-gallery .container');

        animationDiv.slideDown("slow").css('display', 'flex');

        setTimeout(function () {
            animationDiv.slideDown("slow").css('display', 'none');
            $grid.append($elems).masonry('appended', $elems);
        }, 3000);
    });

    function getItemElement() {
        const randomNum = Math.floor(Math.random() * 7) + 1;
        const cloneElement = $('.grid-item:first-child').clone();

        cloneElement.find('img').attr('src', `img/gallery-img${Math.floor(randomNum)}.png`);

        return cloneElement.get(0);
    }
});