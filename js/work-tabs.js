$(document).ready(function () {
    let fActive = '';

    function filterWork(filteBy) {
        if (fActive !== filteBy) {
            $('.wrapper-work > .tabs .tab').removeClass('active');
            $(`.wrapper-work > .tabs .tab.${filteBy}`).addClass('active');

            $('.wrapper-work > .all-list .tabs-content > div').filter('.' + filteBy).show();
            $('.wrapper-work > .all-list .tabs-content > div').filter(':not(.' + filteBy + ')').hide();
            fActive = filteBy;
        }
    }

    $('.wrapper-work > .tabs .gr-des').click(function () {
        filterWork('gr-des');
    });

    $('.wrapper-work > .tabs .web-des').click(function () {
        filterWork('web-des');
    });

    $('.wrapper-work > .tabs .lan-p').click(function () {
        filterWork('lan-p');
    });

    $('.wrapper-work > .tabs .wp').click(function () {
        filterWork('wp');
    });

    $('.wrapper-work > .tabs .all').click(function () {
        $('.wrapper-work > .tabs .tab').removeClass('active');
        $(this).addClass('active');
        $('.wrapper-work > .all-list .tabs-content > div').show();
        fActive = 'all';
    });

    $('.wrapper-work > .btn .load-more').click(function () {
        $('.wrapper-work > .tabs .tab').removeClass('active');
        $('.wrapper-work > .tabs .tab.all').addClass('active');
        $('.wrapper-work > .all-list .tabs-content > div').show();
        fActive = 'all';
    });
});